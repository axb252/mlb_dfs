package bansal;

import java.text.DecimalFormat;

public class Pitcher implements Comparable {

	/*********************************************************************
	 * Class Variables
	 ********************************************************************/
	
	String name;
	double whip, babip, xFIPMinus, siera, kPerc, finalStd;
	int stdSierra, stdXFipMinus, stdKPerc;
	char hand;
	
	
	/*********************************************************************
	 * Constructors
	 ********************************************************************/
	
	public Pitcher(String name, double kPerc, double whip, double babip, double xFIPMinus, double siera) {
		this.name = name;
		this.kPerc = kPerc;
		this.whip = whip;
		this.babip = babip;
		this.xFIPMinus = xFIPMinus;
		this.siera = siera;
		this.finalStd = 0;
	}
	
	/*********************************************************************
	 * Mutators
	 ********************************************************************/
	
	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public double getWhip() {
		return this.whip;
	}


	public void setWhip(double whip) {
		this.whip = whip;
	}


	public double getBabip() {
		return this.babip;
	}


	public void setBabip(double babip) {
		this.babip = babip;
	}


	public double getxFIPMinus() {
		return this.xFIPMinus;
	}


	public void setxFIPMinus(double xFIPMinus) {
		this.xFIPMinus = xFIPMinus;
	}


	public double getSiera() {
		return this.siera;
	}


	public void setSiera(double siera) {
		this.siera = siera;
	}


	public int getStdSierra() {
		return this.stdSierra;
	}


	public void setStdSierra(int stdSierra) {
		this.stdSierra = stdSierra;
	}
	
	public char getHand() {
		return this.hand;
	}

	public void setHand(char hand) {
		this.hand = hand;
	}

	public double getkPerc() {
		return this.kPerc;
	}

	public void setkPerc(double kPerc) {
		this.kPerc = kPerc;
	}
	
	public int getStdXFipMinus() {
		return this.stdXFipMinus;
	}

	public void setStdXFipMinus(int stdXFipMinus) {
		this.stdXFipMinus = stdXFipMinus;
	}

	public int getStdKPerc() {
		return this.stdKPerc;
	}

	public void setStdKPerc(int stdKPerc) {
		this.stdKPerc = stdKPerc;
	}

	public double getFinalStd() {
		return this.finalStd;
	}

	public void setFinalStd(double finalStd) {
		this.finalStd = finalStd;
	}

	/*********************************************************************
	 * User-Defined Methods
	 ********************************************************************/
	
	@Override
	public String toString() {
		DecimalFormat df = new DecimalFormat("#.00");
		//return this.getName() + ": wRAA - " + this.getwRAA() + " | wOBA - " + this.getwOBA() + " | wRC+ - " + this.getwRCPlus();
		//return this.getName() + ": STD Hitting - " + this.getStdwRCPlus();
		//return this.getName() + ": whip - " + this.getWhip() + " | babip - " + this.getBabip() + " | xFIP- - " + this.getxFIPMinus() + " | siera - " + this.getSiera();
		return this.getName() + ": STD Pitching - " + df.format(this.getFinalStd()) + ": STD Sierra - " + this.getStdSierra()
				 + ": STD xFip- - " + this.getStdXFipMinus()  + ": STD K% - " + this.getStdKPerc() + ": K% - " + this.getkPerc();
	}
	
	@Override
	public int compareTo(Object o) {
		Pitcher p = (Pitcher) o;
		if(this.getkPerc() > p.getkPerc()) {
			return -1;
		}
		else if(this.getkPerc() < p.getkPerc()){
			return 1;
		}
		else {
			if(this.getSiera() < p.getSiera()) {
				return -1;
			}
			else if(this.getSiera() > p.getSiera()) {
				return 1;
			}
			else {
				return this.getName().compareTo(p.getName());
			}
		}
		
	}
	
	public void calculateSieraSTD(double mean, double std) {
		double halfStd = std / 2;
		double upperNeutralBound = mean + halfStd;
		double lowerNeutralBound = mean - halfStd;
		double upperFirstStd = upperNeutralBound + std;
		double upperSecondStd = upperNeutralBound + std + std;
		double lowerFirstStd = lowerNeutralBound - std;
		double lowerSecondStd = lowerNeutralBound - std - std;
		if(this.getSiera() >= mean) {
			if(this.getSiera() <= upperNeutralBound){
				this.setStdSierra(0);
			}
			else if(this.getSiera() <= upperFirstStd) {
				this.setStdSierra(-1);
			}
			else if(this.getSiera() <= upperSecondStd) {
				this.setStdSierra(-3);
			}
			else {
				this.setStdSierra(-6);
			}
		}
		else {
			if(this.getSiera() >= lowerNeutralBound){
				this.setStdSierra(0);
			}
			else if(this.getSiera() >= lowerFirstStd) {
				this.setStdSierra(1);
			}
			else if(this.getSiera() >= lowerSecondStd) {
				this.setStdSierra(3);
			}
			else {
				this.setStdSierra(6);
			}
		}
	}
	
	public void calculatexFipMinusSTD(double mean, double std) {
		double halfStd = std / 2;
		double upperNeutralBound = mean + halfStd;
		double lowerNeutralBound = mean - halfStd;
		double upperFirstStd = upperNeutralBound + std;
		double upperSecondStd = upperNeutralBound + std + std;
		double lowerFirstStd = lowerNeutralBound - std;
		double lowerSecondStd = lowerNeutralBound - std - std;
		if(this.getxFIPMinus() >= mean) {
			if(this.getxFIPMinus() <= upperNeutralBound){
				this.setStdXFipMinus(0);
			}
			else if(this.getxFIPMinus() <= upperFirstStd) {
				this.setStdXFipMinus(-1);
			}
			else if(this.getxFIPMinus() <= upperSecondStd) {
				this.setStdXFipMinus(-3);
			}
			else {
				this.setStdXFipMinus(-6);
			}
		}
		else {
			if(this.getxFIPMinus() >= lowerNeutralBound){
				this.setStdXFipMinus(0);
			}
			else if(this.getxFIPMinus() >= lowerFirstStd) {
				this.setStdXFipMinus(1);
			}
			else if(this.getxFIPMinus() >= lowerSecondStd) {
				this.setStdXFipMinus(3);
			}
			else {
				this.setStdXFipMinus(6);
			}
		}
	}
	
	public void calculateKPercSTD(double mean, double std) {
		double halfStd = std / 2;
		double upperNeutralBound = mean + halfStd;
		double lowerNeutralBound = mean - halfStd;
		double upperFirstStd = upperNeutralBound + std;
		double upperSecondStd = upperNeutralBound + std + std;
		double lowerFirstStd = lowerNeutralBound - std;
		double lowerSecondStd = lowerNeutralBound - std - std;
		if(this.getkPerc() >= mean) {
			if(this.getkPerc() <= upperNeutralBound){
				this.setStdKPerc(0);
			}
			else if(this.getkPerc() <= upperFirstStd) {
				this.setStdKPerc(1);
			}
			else if(this.getkPerc() <= upperSecondStd) {
				this.setStdKPerc(3);
			}
			else {
				this.setStdKPerc(6);
			}
		}
		else {
			if(this.getkPerc() >= lowerNeutralBound){
				this.setStdKPerc(0);
			}
			else if(this.getkPerc() >= lowerFirstStd) {
				this.setStdKPerc(-1);
			}
			else if(this.getkPerc() >= lowerSecondStd) {
				this.setStdKPerc(-3);
			}
			else {
				this.setStdKPerc(-6);
			}
		}
	}
	
	public void calculateFinalStd(int stdSierra, int stdstdXFipMinus, int stdKPerc) {
		double sierraStd = stdSierra;
		double xFipMinusStd = stdstdXFipMinus;
		double kPercStd = stdKPerc;
		
		double finalStd = (.5 * kPercStd) + (.3 * sierraStd) + (.2 * xFipMinusStd);
		
		this.setFinalStd(finalStd);
	}
	
}
