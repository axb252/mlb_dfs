package bansal;

import java.text.DecimalFormat;

public class ParkFactor implements Comparable {

	/******************************************
	 * class variables
	 *****************************************/
	
	String name;
	double singleL, singleR, doubleL, doubleR, tripleL, tripleR, homerunL, homerunR;
	double avgL, avgR, avgT, weightedL, weightedR, weightedT;
	int stdL, stdR, stdT;
	
	/******************************************
	 * constructors
	 *****************************************/
	
	public ParkFactor(String name, double singleL, double singleR, double doubleL, double doubleR, double tripleL, double tripleR, double homerunL, double homerunR) {
		this.name = name;
		this.singleL = singleL;
		this.singleR = singleR;
		this.doubleL = doubleL;
		this.doubleR = doubleR;
		this.tripleL = tripleL;
		this.homerunL = homerunL;
		this.homerunR = homerunR;
		avgL = calculateAvgL(singleL, doubleL, tripleL, homerunL);
		avgR = calculateAvgR(singleR, doubleR, tripleR, homerunR);
		avgT = calculateAvgT(singleL, doubleL, tripleL, homerunL, singleR, doubleR, tripleR, homerunR);
		weightedL = calculateWeightedL(singleL, doubleL, tripleL, homerunL);
		weightedR = calculateWeightedR(singleR, doubleR, tripleR, homerunR);
		weightedT = calculateWeightedT(singleL, doubleL, tripleL, homerunL, singleR, doubleR, tripleR, homerunR);
	}

	/******************************************
	 * mutators
	 *****************************************/
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSingleL() {
		return this.singleL;
	}

	public void setSingleL(double singleL) {
		this.singleL = singleL;
	}

	public double getSingleR() {
		return this.singleR;
	}

	public void setSingleR(double singleR) {
		this.singleR = singleR;
	}

	public double getDoubleL() {
		return this.doubleL;
	}

	public void setDoubleL(double doubleL) {
		this.doubleL = doubleL;
	}

	public double getDoubleR() {
		return this.doubleR;
	}

	public void setDoubleR(double doubleR) {
		this.doubleR = doubleR;
	}

	public double getTripleL() {
		return this.tripleL;
	}

	public void setTripleL(double tripleL) {
		this.tripleL = tripleL;
	}

	public double getTripleR() {
		return this.tripleR;
	}

	public void setTripleR(double tripleR) {
		this.tripleR = tripleR;
	}

	public double getHomerunL() {
		return this.homerunL;
	}

	public void setHomerunL(double homerunL) {
		this.homerunL = homerunL;
	}

	public double getHomerunR() {
		return this.homerunR;
	}

	public void setHomerunR(double homerunR) {
		this.homerunR = homerunR;
	}

	public double getAvgL() {
		return this.avgL;
	}

	public void setAvgL(double avgL) {
		this.avgL = avgL;
	}

	public double getAvgR() {
		return this.avgR;
	}

	public void setAvgR(double avgR) {
		this.avgR = avgR;
	}

	public double getAvgT() {
		return this.avgT;
	}

	public void setAvgT(double avgT) {
		this.avgT = avgT;
	}

	public double getWeightedL() {
		return this.weightedL;
	}

	public void setWeightedL(double weightedL) {
		this.weightedL = weightedL;
	}

	public double getWeightedR() {
		return this.weightedR;
	}

	public void setWeightedR(double weightedR) {
		this.weightedR = weightedR;
	}

	public double getWeightedT() {
		return this.weightedT;
	}

	public void setWeightedT(double weightedT) {
		this.weightedT = weightedT;
	}
	
	public int getStdL() {
		return this.stdL;
	}

	public void setStdL(int stdL) {
		this.stdL = stdL;
	}

	public int getStdR() {
		return this.stdR;
	}

	public void setStdR(int stdR) {
		this.stdR = stdR;
	}

	public int getStdT() {
		return this.stdT;
	}

	public void setStdT(int stdT) {
		this.stdT = stdT;
	}

	/******************************************
	 * user-defined methods
	 *****************************************/
	
	public void calculateStds(double leftiesMean, double leftiesStd, double rightiesMean, double rightiesStd, double totalMean, double totalStd) {
		this.calculateStdL(leftiesMean, leftiesStd);
		this.calculateStdR(rightiesMean, rightiesStd);
		this.calculateStdT(totalMean, totalStd);
	}
	
	private void calculateStdL(double leftiesMean, double leftiesStd) {
		double halfStd = leftiesStd / 2;
		double upperNeutralBound = leftiesMean + halfStd;
		double lowerNeutralBound = leftiesMean - halfStd;
		double upperFirstStd = upperNeutralBound + leftiesStd;
		double upperSecondStd = upperNeutralBound + leftiesStd + leftiesStd;
		double lowerFirstStd = lowerNeutralBound - leftiesStd;
		if(this.getWeightedL() >= leftiesMean) {
			if(this.getWeightedL() <= upperNeutralBound){
				this.setStdL(0);
			}
			else if(this.getWeightedL() <= upperFirstStd) {
				this.setStdL(1);
			}
			else if(this.getWeightedL() <= upperSecondStd) {
				this.setStdL(2);
			}
			else {
				this.setStdL(3);
			}
		}
		else {
			if(this.getWeightedL() >= lowerNeutralBound){
				this.setStdL(0);
			}
			else if(this.getWeightedL() >= lowerFirstStd) {
				this.setStdL(-1);
			}
			else {
				this.setStdL(-2);
			}
		}
	}
	
	private void calculateStdR(double rightiesMean, double rightiesStd) {
		double halfStd = rightiesStd / 2;
		double upperNeutralBound = rightiesMean + halfStd;
		double lowerNeutralBound = rightiesMean - halfStd;
		double upperFirstStd = upperNeutralBound + rightiesStd;
		double upperSecondStd = upperNeutralBound + rightiesStd + rightiesStd;
		double lowerFirstStd = lowerNeutralBound - rightiesStd;
		if(this.getWeightedR() >= rightiesMean) {
			if(this.getWeightedR() <= upperNeutralBound){
				this.setStdR(0);
			}
			else if(this.getWeightedR() <= upperFirstStd) {
				this.setStdR(1);
			}
			else if(this.getWeightedR() <= upperSecondStd) {
				this.setStdR(2);
			}
			else {
				this.setStdR(3);
			}
		}
		else {
			if(this.getWeightedR() >= lowerNeutralBound){
				this.setStdR(0);
			}
			else if(this.getWeightedR() >= lowerFirstStd) {
				this.setStdR(-1);
			}
			else {
				this.setStdR(-2);
			}
		}
	}

	private void calculateStdT(double totalMean, double totalStd) {
		double halfStd = totalStd / 2;
		double upperNeutralBound = totalMean + halfStd;
		double lowerNeutralBound = totalMean - halfStd;
		double upperFirstStd = upperNeutralBound + totalStd;
		double upperSecondStd = upperNeutralBound + totalStd + totalStd;
		double lowerFirstStd = lowerNeutralBound - totalStd;
		if(this.getWeightedT() >= totalMean) {
			if(this.getWeightedT() <= upperNeutralBound){
				this.setStdT(0);
			}
			else if(this.getWeightedT() <= upperFirstStd) {
				this.setStdT(1);
			}
			else if(this.getWeightedT() <= upperSecondStd) {
				this.setStdT(3);
			}
			else {
				this.setStdT(6);
			}
		}
		else {
			if(this.getWeightedT() >= lowerNeutralBound){
				this.setStdT(0);
			}
			else if(this.getWeightedT() >= lowerFirstStd) {
				this.setStdT(-1);
			}
			else {
				this.setStdT(-3);
			}
		}
	}
	
	private double calculateWeightedT(double singleL, double doubleL, double tripleL,
			double homerunL, double singleR, double doubleR, double tripleR,
			double homerunR) {
		return ((singleL * .1) + (doubleL * .2) + (tripleL*.3) + (homerunL*.4) + (singleR*.1) + (doubleR*.2) + (tripleR*.3) + (homerunR*.4)) / 2;
		
	}

	private double calculateWeightedR(double singleR, double doubleR, double tripleR,
			double homerunR) {
		return ((singleR * .1) + (doubleR * .2) + (tripleR * .3) + (homerunR * .4)) ;
		
	}


	private double calculateWeightedL(double singleL, double doubleL, double tripleL,
			double homerunL) {
		return ((singleL * .1) + (doubleL * .2) + (tripleL * .3) + (homerunL * .4)) ;
		
	}


	private double calculateAvgT(double singleL, double doubleL, double tripleL,
			double homerunL, double singleR, double doubleR, double tripleR,
			double homerunR) {
		return (singleL + doubleL + tripleL + homerunL + singleR + doubleR + tripleR + homerunR) / 8;
		
	}


	private double calculateAvgR(double singleR, double doubleR, double tripleR,
			double homerunR) {
		return (singleR + doubleR + tripleR + homerunR) / 4;
		
	}


	private double calculateAvgL(double singleL, double doubleL, double tripleL,
			double homerunL) {
		return (singleL + doubleL + tripleL + homerunL) / 4;
		
	}

	@Override
	public int compareTo(Object o) {
		ParkFactor p = (ParkFactor) o;
		if(this.getWeightedT() > p.getWeightedT()) {
			return -1;
		}
		else if(this.getWeightedT() < p.getWeightedT()) {
			return 1;
		}
		else {
			return this.getName().compareTo(p.getName());
		}
	}

	@Override
	public String toString() {
		DecimalFormat df = new DecimalFormat("#.00");
		return this.getName() + ": STD Lefties - " + df.format(this.getStdL()) + " | STD Righties - " + df.format(this.getStdR()) + " | STD Total - " + df.format(this.getStdT());
	}

	
	
}
