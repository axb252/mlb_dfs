package bansal;

public class Team implements Comparable {

	/****************************************************************
	 * Class Variables
	 ***************************************************************/
	
	String name;
	double wRCPlus, wOBA, wRAA;
	int stdwRCPlus;
	
	
	/****************************************************************
	 * Constructors
	 ***************************************************************/
	
	public Team(String name, double wRCPlus, double wOBA, double wRAA) {
		this.name = name;
		this.wRCPlus = wRCPlus;
		this.wOBA = wOBA;
		this.wRAA = wRAA;
	}
	
	/****************************************************************
	 * Mutators
	 ***************************************************************/
	
	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public double getwRCPlus() {
		return this.wRCPlus;
	}


	public void setwRCPlus(double wRCPlus) {
		this.wRCPlus = wRCPlus;
	}


	public double getwOBA() {
		return this.wOBA;
	}


	public void setwOBA(double wOBA) {
		this.wOBA = wOBA;
	}


	public double getwRAA() {
		return this.wRAA;
	}


	public void setwRAA(double wRAA) {
		this.wRAA = wRAA;
	}


	public int getStdwRCPlus() {
		return this.stdwRCPlus;
	}


	public void setStdwRCPlus(int stdwRCPlus) {
		this.stdwRCPlus = stdwRCPlus;
	}

	
	/****************************************************************
	 * User-Defined Methods
	 ***************************************************************/
	
	@Override
	public String toString() {
		//return this.getName() + ": wRAA - " + this.getwRAA() + " | wOBA - " + this.getwOBA() + " | wRC+ - " + this.getwRCPlus();
		return this.getName() + ": STD Hitting - " + this.getStdwRCPlus();
	}
	
	public String toString2() {
		return this.getName() + ": wRAA - " + this.getwRAA() + " | wOBA - " + this.getwOBA() + " | wRC+ - " + this.getwRCPlus();
		//return this.getName() + ": STD Hitting - " + this.getStdwRCPlus();
	}
	
	@Override
	public int compareTo(Object o) {
		Team t = (Team) o;
		if(this.getwRCPlus() > t.getwRCPlus()) {
			return -1;
		}
		else if(this.getwRCPlus() < t.getwRCPlus()) {
			return 1;
		}
		else {
			return this.getName().compareTo(t.getName());
		}
	}
	
	public void calculateWRCPlusSTD(double mean, double std) {
		double halfStd = std / 2;
		double upperNeutralBound = mean + halfStd;
		double lowerNeutralBound = mean - halfStd;
		double upperFirstStd = upperNeutralBound + std;
		double upperSecondStd = upperNeutralBound + std + std;
		double lowerFirstStd = lowerNeutralBound - std;
		double lowerSecondStd = lowerNeutralBound - std - std;
		if(this.getwRCPlus() >= mean) {
			if(this.getwRCPlus() <= upperNeutralBound){
				this.setStdwRCPlus(0);
			}
			else if(this.getwRCPlus() <= upperFirstStd) {
				this.setStdwRCPlus(1);
			}
			else if(this.getwRCPlus() <= upperSecondStd) {
				this.setStdwRCPlus(3);
			}
			else {
				this.setStdwRCPlus(6);
			}
		}
		else {
			if(this.getwRCPlus() >= lowerNeutralBound){
				this.setStdwRCPlus(0);
			}
			else if(this.getwRCPlus() >= lowerFirstStd) {
				this.setStdwRCPlus(-1);
			}
			else if(this.getwRCPlus() >= lowerSecondStd) {
				this.setStdwRCPlus(-3);
			}
			else {
				this.setStdwRCPlus(-6);
			}
		}
	}
	
}
