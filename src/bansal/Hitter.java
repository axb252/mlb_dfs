package bansal;

import java.text.DecimalFormat;

public class Hitter implements Comparable {
	
	/*********************************************************************
	 * Class Variables
	 ********************************************************************/
	
	String name;
	double wRCPlus, wOBA, wRAA;
	double wRCPlusVSL, wOBAVSL, wRAAVSL;
	double wRCPlusVSR, wOBAVSR, wRAAVSR;
	double finalwRCPlusDaily, finalMatchupValue;
	int stdwRCPlus, stdwRCPlusVSL, stdwRCPlusVSR;
	boolean setstdwRCPlusVSL, setstdwRCPlusVSR;
	boolean wasFinalCalculationDone;
	char swings;
	String position, time;
	
	/*********************************************************************
	 * Constructors
	 ********************************************************************/
	
	public Hitter(String name, double wRCPlus, double wOBA, double wRAA) {
		this.name = name;
		this.wRCPlus = wRCPlus;
		this.wOBA = wOBA;
		this.wRAA = wRAA;
		this.setstdwRCPlusVSL = false;
		this.setstdwRCPlusVSR = false;
		this.wasFinalCalculationDone = false;
		this.position = "null";
		this.finalMatchupValue = 0;
	}
	
	/*********************************************************************
	 * Mutators
	 ********************************************************************/
	
	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public double getwRCPlus() {
		return this.wRCPlus;
	}


	public void setwRCPlus(double wRCPlus) {
		this.wRCPlus = wRCPlus;
	}


	public double getwOBA() {
		return this.wOBA;
	}


	public void setwOBA(double wOBA) {
		this.wOBA = wOBA;
	}


	public double getwRAA() {
		return this.wRAA;
	}


	public void setwRAA(double wRAA) {
		this.wRAA = wRAA;
	}


	public int getStdwRCPlus() {
		return this.stdwRCPlus;
	}


	public void setStdwRCPlus(int stdwRCPlus) {
		this.stdwRCPlus = stdwRCPlus;
	}
	
	public char getSwings() {
		return this.swings;
	}

	public void setSwings(char swings) {
		this.swings = swings;
	}

	public double getwRCPlusVSL() {
		return this.wRCPlusVSL;
	}

	public void setwRCPlusVSL(double wRCPlusVSL) {
		this.wRCPlusVSL = wRCPlusVSL;
		this.setstdwRCPlusVSL = true;
	}

	public double getwOBAVSL() {
		return this.wOBAVSL;
	}

	public void setwOBAVSL(double wOBAVSL) {
		this.wOBAVSL = wOBAVSL;
	}

	public double getwRAAVSL() {
		return this.wRAAVSL;
	}

	public void setwRAAVSL(double wRAAVSL) {
		this.wRAAVSL = wRAAVSL;
	}

	public double getwRCPlusVSR() {
		return this.wRCPlusVSR;
	}

	public void setwRCPlusVSR(double wRCPlusVSR) {
		this.wRCPlusVSR = wRCPlusVSR;
		this.setstdwRCPlusVSR = true;
	}

	public double getwOBAVSR() {
		return this.wOBAVSR;
	}

	public void setwOBAVSR(double wOBAVSR) {
		this.wOBAVSR = wOBAVSR;
	}

	public double getwRAAVSR() {
		return this.wRAAVSR;
	}

	public void setwRAAVSR(double wRAAVSR) {
		this.wRAAVSR = wRAAVSR;
	}

	public int getStdwRCPlusVSL() {
		return this.stdwRCPlusVSL;
	}

	public void setStdwRCPlusVSL(int stdwRCPlusVSL) {
		this.stdwRCPlusVSL = stdwRCPlusVSL;
	}

	public int getStdwRCPlusVSR() {
		return this.stdwRCPlusVSR;
	}

	public void setStdwRCPlusVSR(int stdwRCPlusVSR) {
		this.stdwRCPlusVSR = stdwRCPlusVSR;
	}

	public boolean isSetstdwRCPlusVSL() {
		return this.setstdwRCPlusVSL;
	}

	public void setSetstdwRCPlusVSL(boolean setstdwRCPlusVSL) {
		this.setstdwRCPlusVSL = setstdwRCPlusVSL;
	}

	public boolean isSetstdwRCPlusVSR() {
		return this.setstdwRCPlusVSR;
	}

	public void setSetstdwRCPlusVSR(boolean setstdwRCPlusVSR) {
		this.setstdwRCPlusVSR = setstdwRCPlusVSR;
	}
	
	public double getFinalwRCPlusDaily() {
		return this.finalwRCPlusDaily;
	}

	public void setFinalwRCPlusDaily(double finalwRCPlusDaily) {
		this.finalwRCPlusDaily = finalwRCPlusDaily;
	}
	
	public String getPosition() {
		return this.position;
	}

	public void setPosition(String position) {
		if(position.equals("LF") || position.equals("RF") || position.equals("CF")){
			this.position = "OF";
		}
		else {
			this.position = position;
		}
	}

	public boolean isWasFinalCalculationDone() {
		return this.wasFinalCalculationDone;
	}

	public void setWasFinalCalculationDone(boolean wasFinalCalculationDone) {
		this.wasFinalCalculationDone = wasFinalCalculationDone;
	}

	public double getFinalMatchupValue() {
		return this.finalMatchupValue;
	}

	public void setFinalMatchupValue(double finalMatchupValue) {
		this.finalMatchupValue = finalMatchupValue;
	}

	public String getTime() {
		return this.time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	/*********************************************************************
	 * User-Defined Methods
	 ********************************************************************/
	
	@Override
	public String toString() {
		DecimalFormat df = new DecimalFormat("#.00");
		//return this.getName() + ": wRAA - " + this.getwRAA() + " | wOBA - " + this.getwOBA() + " | wRC+ - " + this.getwRCPlus();
		return this.getName() + " | " + this.getTime() + " | " + this.getPosition() + " Daily wRC+ - " + df.format(this.getFinalwRCPlusDaily()) + " Final Matchup value - " + df.format(this.getFinalMatchupValue()) + " | STD Hitting - " + this.getStdwRCPlus() + " | STD Hitting VSL- " + this.getStdwRCPlusVSL() + " | STD Hitting VSR- " + this.getStdwRCPlusVSR();
	}

	public String toString2() {
		return this.getName() + ": wRAA - " + this.getwRAA() + " | wOBA - " + this.getwOBA() + " | wRC+ - " + this.getwRCPlus() + " | wRCVSL+ - " + this.getwRCPlusVSL() + " | wRCVSR+ - " + this.getwRCPlusVSR();
		//return this.getName() + ": STD Hitting - " + this.getStdwRCPlus();
	}
	
	@Override
	public int compareTo(Object o) {
		Hitter h = (Hitter) o;
		if(this.isWasFinalCalculationDone()){
			if(this.getPosition().compareTo(h.getPosition()) > 0) {
				return 1;
			}
			else if(this.getPosition().compareTo(h.getPosition()) < 0) {
				return -1;
			}
			else {
				if(this.getFinalMatchupValue() > h.getFinalMatchupValue()) {
					return -1;
				}
				else if(this.getFinalMatchupValue() < h.getFinalMatchupValue()) {
					return 1;
				}
				else {
					return this.getName().compareTo(h.getName());
				}
			}
			
		}
		else {
			if(this.getPosition().compareTo(h.getPosition()) > 0) {
				return 1;
			}
			else if(this.getPosition().compareTo(h.getPosition()) < 0) {
				return -1;
			}
			else {
				if(this.getwRCPlus() > h.getwRCPlus()) {
					return -1;
				}
				else if(this.getwRCPlus() < h.getwRCPlus()) {
					return 1;
				}
				else {
					return this.getName().compareTo(h.getName());
				}
			}
			
		}
		
	}
	
	public void calculateWRCPlusSTD(double mean, double std) {
		double halfStd = std / 2.0;
		double upperNeutralBound = mean + halfStd;
		double lowerNeutralBound = mean - halfStd;
		double upperFirstStd = upperNeutralBound + std;
		double upperSecondStd = upperNeutralBound + std + std;
		double upperThirdStd = upperNeutralBound + std + std + std;
		double lowerFirstStd = lowerNeutralBound - std;
		double lowerSecondStd = lowerNeutralBound - std - std;
		double lowerThirdStd = lowerNeutralBound - std - std - std;
		if(this.getwRCPlus() >= mean) {
			if(this.getwRCPlus() <= upperNeutralBound){
				this.setStdwRCPlus(0);
			}
			else if(this.getwRCPlus() <= upperFirstStd) {
				this.setStdwRCPlus(1);
			}
			else if(this.getwRCPlus() <= upperSecondStd) {
				this.setStdwRCPlus(3);
			}
			else if(this.getwRCPlus() <= upperThirdStd) {
				this.setStdwRCPlus(6);
			}
			else {
				this.setStdwRCPlus(10);
			}
		}
		else {
			if(this.getwRCPlus() >= lowerNeutralBound){
				this.setStdwRCPlus(0);
			}
			else if(this.getwRCPlus() >= lowerFirstStd) {
				this.setStdwRCPlus(-1);
			}
			else if(this.getwRCPlus() >= lowerSecondStd) {
				this.setStdwRCPlus(-3);
			}
			else if(this.getwRCPlus() >= lowerThirdStd) {
				this.setStdwRCPlus(-6);
			}
			else {
				this.setStdwRCPlus(-10);
			}
		}
	}
	
	public void calculateWRCPlusSTDVSR(double mean, double std) {
		double halfStd = std / 2.0;
		double upperNeutralBound = mean + halfStd;
		double lowerNeutralBound = mean - halfStd;
		double upperFirstStd = upperNeutralBound + std;
		double upperSecondStd = upperNeutralBound + std + std;
		double upperThirdStd = upperNeutralBound + std + std + std;
		double lowerFirstStd = lowerNeutralBound - std;
		double lowerSecondStd = lowerNeutralBound - std - std;
		double lowerThirdStd = lowerNeutralBound - std - std - std;
		if(this.getwRCPlusVSR() >= mean) {
			if(this.getwRCPlusVSR() <= upperNeutralBound){
				this.setStdwRCPlusVSR(0);
			}
			else if(this.getwRCPlusVSR() <= upperFirstStd) {
				this.setStdwRCPlusVSR(1);
			}
			else if(this.getwRCPlusVSR() <= upperSecondStd) {
				this.setStdwRCPlusVSR(3);
			}
			else if(this.getwRCPlusVSR() <= upperThirdStd) {
				this.setStdwRCPlusVSR(6);
			}
			else {
				this.setStdwRCPlusVSR(10);
			}
		}
		else {
			if(this.getwRCPlusVSR() >= lowerNeutralBound){
				this.setStdwRCPlusVSR(0);
			}
			else if(this.getwRCPlusVSR() >= lowerFirstStd) {
				this.setStdwRCPlusVSR(-1);
			}
			else if(this.getwRCPlusVSR() >= lowerSecondStd) {
				this.setStdwRCPlusVSR(-3);
			}
			else if(this.getwRCPlusVSR() >= lowerThirdStd) {
				this.setStdwRCPlusVSR(-6);
			}
			else {
				this.setStdwRCPlusVSR(-10);
			}
		}
	}
	
	public void calculateWRCPlusSTDVSL(double mean, double std) {
		double halfStd = std / 2.0;
		double upperNeutralBound = mean + halfStd;
		double lowerNeutralBound = mean - halfStd;
		double upperFirstStd = upperNeutralBound + std;
		double upperSecondStd = upperNeutralBound + std + std;
		double upperThirdStd = upperNeutralBound + std + std + std;
		double lowerFirstStd = lowerNeutralBound - std;
		double lowerSecondStd = lowerNeutralBound - std - std;
		double lowerThirdStd = lowerNeutralBound - std - std - std;
		if(this.getwRCPlusVSL() >= mean) {
			if(this.getwRCPlusVSL() <= upperNeutralBound){
				this.setStdwRCPlusVSL(0);
			}
			else if(this.getwRCPlusVSL() <= upperFirstStd) {
				this.setStdwRCPlusVSL(1);
			}
			else if(this.getwRCPlusVSL() <= upperSecondStd) {
				this.setStdwRCPlusVSL(3);
			}
			else if(this.getwRCPlusVSL() <= upperThirdStd) {
				this.setStdwRCPlusVSL(6);
			}
			else {
				this.setStdwRCPlusVSL(10);
			}
		}
		else {
			if(this.getwRCPlusVSL() >= lowerNeutralBound){
				this.setStdwRCPlusVSL(0);
			}
			else if(this.getwRCPlusVSL() >= lowerFirstStd) {
				this.setStdwRCPlusVSL(-1);
			}
			else if(this.getwRCPlusVSL() >= lowerSecondStd) {
				this.setStdwRCPlusVSL(-3);
			}
			else if(this.getwRCPlusVSL() >= lowerThirdStd) {
				this.setStdwRCPlusVSL(-6);
			}
			else {
				this.setStdwRCPlusVSL(-10);
			}
		}
	}
	
	public double calculateFinalwRCPlusDaily (int stdwRCPlus, int stdwRCPlusHand){
		
		double stdwRCPlusDec = stdwRCPlus;
		double stdwRCPlusHandDec = stdwRCPlusHand;
		double finalValue = (.7 * stdwRCPlusDec) + (.3 * stdwRCPlusHandDec);
		this.setFinalwRCPlusDaily(finalValue);
		this.setWasFinalCalculationDone(true);
		return finalValue;
	}

}
