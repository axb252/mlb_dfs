package bansal;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

public class FinalNumberObject implements Comparable {
	
	/*********************************************************************
	 * Class Variables
	 ********************************************************************/
	
	String name;
	ArrayList<Object> value;
	
	/*********************************************************************
	 * Constructors
	 ********************************************************************/
	
	public FinalNumberObject(String name, double value) {
		this.name = name;
		this.value.add(value);
		
	}
	
	public FinalNumberObject(String name, ArrayList<Object> value) {
		this.name = name;
		this.value = value;
		
	}

	
	/*********************************************************************
	 * Mutators
	 ********************************************************************/
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Object> getValue() {
		return this.value;
	}

	public void setValue(ArrayList<Object> value) {
		this.value = value;
	}

	
	/*********************************************************************
	 * User-Defined Methods
	 ********************************************************************/
	
	@Override
	public String toString() {
		DecimalFormat df = new DecimalFormat("#.00");
		String values = "";
		for(Object d : this.getValue()) {
			if(d instanceof Double){
				values += df.format(d) + " | ";
			}
			else {
				values += d.toString() + " | ";
			}
			
		}
		return name + ": " + values;
	}


	@Override
	public int compareTo(Object o) {
		FinalNumberObject f = (FinalNumberObject) o;
//		DateFormat formatter = new SimpleDateFormat("hh:mm a");
//		Date date = null;
//		try {
//			date = (Date)formatter.parse(this.getValue().get(this.size()-1).toString());
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		Date dateF = null;
//		try {
//			dateF = (Date)formatter.parse(f.getValue().get(this.size()-1).toString());
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		if(date.compareTo(dateF) > 0) {
//			return 1;
//		}
//		else if(date.compareTo(dateF) < 0) {
//			return -1;
//		}
//		else {
			if((Double)this.getValue().get(0) > (Double)f.getValue().get(0)) {
				return -1;
			}
			else if((Double)this.getValue().get(0) < (Double)f.getValue().get(0)) {
				return 1;
			}
			else {
				return this.getName().compareTo(f.getName());
			}
		//}
	}
	
	public int size() {
		return this.getValue().size();
	}
	
	public void addValue(Object o) {
		this.getValue().add(o);
	}

}
