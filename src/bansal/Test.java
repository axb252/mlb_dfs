package bansal;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Test {

	public static void main(String[] args) {
		Document doc;
		try {
			doc = Jsoup.connect("http://www.rotowire.com/baseball/daily_lineups.htm").get();
			Elements tableRowTime = doc.getElementsByClass("dlineups-topboxcenter-topline");
			Elements tableRow = doc.getElementsByClass("dlineups-mainbox");
			int counter = 0;
			for(int i = 0; i < tableRow.size();i++) {
				//Element div = tableRow.get(i).select("div").first();
				String time = tableRowTime.get(i).select("div").first().child(0).text().substring(0, tableRowTime.get(i).select("div").first().child(0).text().indexOf("M") + 1);
				DateFormat formatter = new SimpleDateFormat("hh:mm a");
				Date date = (Date)formatter.parse(time);
				String timeDisplay = date.toInstant().atZone(ZoneId.systemDefault()).toString().substring(date.toInstant().atZone(ZoneId.systemDefault()).toString().indexOf("T")+1, date.toInstant().atZone(ZoneId.systemDefault()).toString().indexOf("["));
				System.out.println(date.toInstant().atZone(ZoneId.systemDefault()).toString().substring(date.toInstant().atZone(ZoneId.systemDefault()).toString().indexOf("T")+1, date.toInstant().atZone(ZoneId.systemDefault()).toString().indexOf("[")));
				counter++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
