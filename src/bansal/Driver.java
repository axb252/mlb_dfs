package bansal;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.text.TableView.TableRow;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Driver {

	public static void main(String[] args) {
		
		TreeSet<ParkFactor> set = new TreeSet<ParkFactor>();
		TreeSet<Team> teamSet = new TreeSet<Team>();
		TreeSet<Pitcher> pitcherSet = new TreeSet<Pitcher>();
		TreeSet<FinalNumberObject> finalPitcherSet = new TreeSet<FinalNumberObject>();
		TreeSet<Hitter> finalHitterSet= new TreeSet<Hitter>();
		TreeSet<Hitter> eliteHitterSet = new TreeSet<Hitter>();
		TreeSet<FinalNumberObject> finalTeamSet = new TreeSet<FinalNumberObject>();
		HashMap<String, Integer> mapPF = new HashMap<String, Integer>();
		HashMap<String, Integer> mapTeamO = new HashMap<String, Integer>();
		HashMap<String, Double> mapPitcher = new HashMap<String, Double>();
		HashMap<String, Hitter> mapHitter = new HashMap<String, Hitter>();
		
		HashMap<String, String> teamNameMapping = new HashMap<String, String>();
		teamNameMapping.put("CLE", "Indians");
		teamNameMapping.put("BAL", "Orioles");
		teamNameMapping.put("ATL", "Braves");
		teamNameMapping.put("PIT", "Pirates");
		teamNameMapping.put("WAS", "Nationals");
		teamNameMapping.put("PHI", "Phillies");
		teamNameMapping.put("TEX", "Rangers");
		teamNameMapping.put("TOR", "Blue Jays");
		teamNameMapping.put("CWS", "White Sox");
		teamNameMapping.put("DET", "Tigers");
		teamNameMapping.put("CIN", "Reds");
		teamNameMapping.put("NYM", "Mets");
		teamNameMapping.put("LAD", "Dodgers");
		teamNameMapping.put("MIA", "Marlins");
		teamNameMapping.put("BOS", "Red Sox");
		teamNameMapping.put("TB", "Rays");
		teamNameMapping.put("MIN", "Twins");
		teamNameMapping.put("MIL", "Brewers");
		teamNameMapping.put("NYY", "Yankees");
		teamNameMapping.put("HOU", "Astros");
		teamNameMapping.put("CHC", "Cubs");
		teamNameMapping.put("STL", "Cardinals");
		teamNameMapping.put("SEA", "Mariners");
		teamNameMapping.put("LAA", "Angels");
		teamNameMapping.put("KC", "Royals");
		teamNameMapping.put("OAK", "Athletics");
		teamNameMapping.put("ARI", "Diamondbacks");
		teamNameMapping.put("SD", "Padres");
		teamNameMapping.put("COL", "Rockies");
		teamNameMapping.put("SF", "Giants");
		teamNameMapping.put("Washington Nationals", "Nationals");
		teamNameMapping.put("Philadelphia Phillies", "Phillies");
		teamNameMapping.put("Texas Rangers", "Rangers");
		teamNameMapping.put("Toronto Blue Jays", "Blue Jays");
		teamNameMapping.put("Chicago White Sox", "White Sox");
		teamNameMapping.put("Detroit Tigers", "Tigers");
		teamNameMapping.put("Cincinnati Reds", "Reds");
		teamNameMapping.put("New York Mets", "Mets");
		teamNameMapping.put("Los Angeles Dodgers", "Dodgers");
		teamNameMapping.put("Miami Marlins", "Marlins");
		teamNameMapping.put("Boston Red Sox", "Red Sox");
		teamNameMapping.put("Tampa Bay Rays", "Rays");
		teamNameMapping.put("Atlanta Braves", "Braves");
		teamNameMapping.put("Pittsburgh Pirates", "Pirates");
		teamNameMapping.put("Minnesota Twins", "Twins");
		teamNameMapping.put("Milwaukee Brewers", "Brewers");
		teamNameMapping.put("New York Yankees", "Yankees");
		teamNameMapping.put("Houston Astros", "Astros");
		teamNameMapping.put("Seattle Mariners", "Mariners");
		teamNameMapping.put("Los Angeles Angels", "Angels");
		teamNameMapping.put("Colorado Rockies", "Rockies");
		teamNameMapping.put("San Francisco Giants", "Giants");
		teamNameMapping.put("Kansas City Royals", "Royals");
		teamNameMapping.put("Oakland A\'s", "Athletics");
		teamNameMapping.put("Arizona Diamondbacks", "Diamondbacks");
		teamNameMapping.put("San Diego Padres", "Padres");
		teamNameMapping.put("Cleveland Indians", "Indians");
		teamNameMapping.put("Baltimore Orioles", "Orioles");
		teamNameMapping.put("Chicago Cubs", "Cubs");
		teamNameMapping.put("St. Louis Cardinals", "Cardinals");
		
		
		//testmethod();
		getParkFactors(set);
		getPFStats(set);
		
		getTeamHittingStats(teamSet);
		calculateTeamHittingDeviations(teamSet);
		
		getPitcherStats(pitcherSet);
		calculatePitcherDeviations(pitcherSet);
		
//		for(Pitcher p : pitcherSet) {
//			System.out.println(p.toString());
//		}
		getHitterStats(mapHitter);
		calculateHitterDeviations(mapHitter);
		
		
		/* Iterate through a set and print it 
		 Iterator<Pitcher> it = pitcherSet.iterator();
		while(it.hasNext()) {
			System.out.println(it.next().toString());
		}
		*/
		
		for(ParkFactor p : set) {
			mapPF.put(p.getName(), p.getStdT());
		}
		
		for(Team t : teamSet) {
			mapTeamO.put(t.getName(), t.getStdwRCPlus());
		}
		
		for(Pitcher p : pitcherSet) {
			mapPitcher.put(p.getName(), p.getFinalStd());
		}
		
		//printMap(mapPF);
		/*for (Map.Entry<String, Hitter> entry : mapHitter.entrySet()) {
		    System.out.println(entry.getValue().toString());
		}*/
		
		calculateMatchUpEfficiency(mapPF, mapTeamO, mapPitcher, teamNameMapping, mapHitter, finalPitcherSet, finalTeamSet, finalHitterSet);
		
		for(FinalNumberObject f : finalPitcherSet){
			System.out.println(f.toString());
		}
		System.out.println();
		System.out.println("-----------------------------------------");
		System.out.println();
		for(FinalNumberObject f : finalTeamSet){
			System.out.println(f.toString());
		}
		//System.out.println();
		//System.out.println("-----------------------------------------");
		//System.out.println();
		for(Hitter h : finalHitterSet){
			//System.out.println(h.toString());
			if(h.getFinalwRCPlusDaily() >= 1.4) {
				eliteHitterSet.add(h);
			}
			if(h.getFinalMatchupValue() >= 1.5 && h.getPosition().equals("SS")) {
				eliteHitterSet.add(h);
			}
			if(h.getFinalMatchupValue() >=2.2) {
				eliteHitterSet.add(h);
			}
		}	
		System.out.println();
		System.out.println("-----------------------------------------");
		System.out.println();
		for(Hitter h : eliteHitterSet) {
			System.out.println(h.toString());
		}
	}

	private static void testmethod() {
		Document doc;
		try {
			doc = Jsoup.connect("http://www.rotowire.com/baseball/daily_lineups.htm").get();
			Elements tableRow = doc.getElementsByClass("dlineups-mainbox");
			for(int i = 0; i < tableRow.size();i++){
				System.out.println(tableRow.get(i));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private static void calculateHitterDeviations(HashMap<String, Hitter> mapHitter) {
		DescriptiveStatistics stats = new DescriptiveStatistics();
		DescriptiveStatistics statsVSL = new DescriptiveStatistics();
		DescriptiveStatistics statsVSR = new DescriptiveStatistics();

		for (Map.Entry<String, Hitter> entry : mapHitter.entrySet()) {
		    stats.addValue(entry.getValue().getwRCPlus());
			statsVSL.addValue(entry.getValue().getwRCPlusVSL());
			statsVSR.addValue(entry.getValue().getwRCPlusVSR());
		}

		double mean = stats.getMean();
		double std = stats.getStandardDeviation();
		double meanVSL = statsVSL.getMean();
		double stdVSL = statsVSL.getStandardDeviation();
		double meanVSR = statsVSR.getMean();
		double stdVSR = statsVSR.getStandardDeviation();

		for (Map.Entry<String, Hitter> entry : mapHitter.entrySet()) {
			entry.getValue().calculateWRCPlusSTD(mean, std);
			if(entry.getValue().isSetstdwRCPlusVSL()){
				entry.getValue().calculateWRCPlusSTDVSL(meanVSL, stdVSL);
			}
			else {
				entry.getValue().setStdwRCPlusVSL(0);
			}
			if(entry.getValue().isSetstdwRCPlusVSR()){
				entry.getValue().calculateWRCPlusSTDVSR(meanVSR, stdVSR);
			}
			else {
				entry.getValue().setStdwRCPlusVSR(0);
			}
		}
			
		
	}

	private static void getHitterStats(HashMap<String, Hitter> mapHitter) {
		try {
			Document doc = Jsoup.connect("http://www.fangraphs.com/leaders.aspx?pos=all&stats=bat&lg=all&qual=20&type=1&season=2015&month=0&season1=2015&ind=0&team=0&rost=0&age=0&filter=&players=0&page=1_2000").get();
			Elements tableRow = doc.getElementsByClass("rgRow");
			Elements tableRow2 = doc.getElementsByClass("rgAltRow");
			tableRow.addAll(tableRow2);
			for(int i = 0; i < tableRow.size(); i++){
				Hitter h = new Hitter(tableRow.get(i).child(1).text(), Double.parseDouble(tableRow.get(i).child(20).text()), Double.parseDouble(tableRow.get(i).child(19).text()), Double.parseDouble(tableRow.get(i).child(18).text()));
				mapHitter.put(h.getName(), h);
			}
			Document docVSL = Jsoup.connect("http://www.fangraphs.com/leaders.aspx?pos=all&stats=bat&lg=all&qual=20&type=1&season=2015&month=13&season1=2015&ind=0&team=0&rost=0&age=0&filter=&players=0&sort=17,d&page=1_1000").get();
			Elements tableRowVSL = docVSL.getElementsByClass("rgRow");
			Elements tableRow2VSL = docVSL.getElementsByClass("rgAltRow");
			tableRowVSL.addAll(tableRow2VSL);
			for(int i = 0; i < tableRowVSL.size(); i++){
				//System.out.println(tableRowVSL.get(i).child(1).text());
				mapHitter.get(tableRowVSL.get(i).child(1).text()).setwRCPlusVSL(Double.parseDouble(tableRowVSL.get(i).child(17).text()));;
			}
			Document docVSR = Jsoup.connect("http://www.fangraphs.com/leaders.aspx?pos=all&stats=bat&lg=all&qual=30&type=1&season=2015&month=14&season1=2015&ind=0&team=0&rost=0&age=0&filter=&players=0&page=1_1000").get();
			Elements tableRowVSR = docVSR.getElementsByClass("rgRow");
			Elements tableRow2VSR = docVSR.getElementsByClass("rgAltRow");
			tableRowVSR.addAll(tableRow2VSR);
			for(int i = 0; i < tableRowVSR.size(); i++){
				mapHitter.get(tableRowVSR.get(i).child(1).text()).setwRCPlusVSR(Double.parseDouble(tableRowVSR.get(i).child(17).text()));;
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private static void calculateMatchUpEfficiency(HashMap<String, Integer> mapPF, HashMap<String, Integer> mapTeamO, HashMap<String, Double> mapPitcher, HashMap<String, String> teamNameMapping, HashMap<String, Hitter> mapHitter, TreeSet<FinalNumberObject> finalPitcherSet, TreeSet<FinalNumberObject> finalTeamSet, TreeSet<Hitter> finalHitterSet) {
		
		try {
			Document doc = Jsoup.connect("http://www.rotowire.com/baseball/daily_lineups.htm").get();
			Elements tableRow = doc.getElementsByClass("dlineups-mainbox");
			Elements tableRowTime = doc.getElementsByClass("dlineups-topboxcenter-topline");
//			for(int i = 0;i<tableRow.size();i++){
//				System.out.println(tableRow.get(i));
//			}
			for(int i = 0; i < tableRow.size();i++) {
				Element div = tableRow.get(i).select("div").first();
				String time = tableRowTime.get(i).select("div").first().child(0).text().substring(0, tableRowTime.get(i).select("div").first().child(0).text().indexOf("M")+1);
				
				
				String awayTeam = teamNameMapping.get(div.child(0).child(0).text());
				String homeTeam = teamNameMapping.get(div.child(0).child(1).text().substring(2, div.child(0).child(1).text().length()));
				String park = homeTeam;
				
				String awayPitcher;
				String homePitcher;
				char awayPitcherHand='R';
				char homePitcherHand='R';
				try {
					awayPitcher = div.child(2).child(0).child(0).child(0).text();
					awayPitcherHand = div.child(2).child(0).child(0).child(1).text().charAt(2);
				} catch (Exception e) {
					awayPitcher = "N/A";
					awayPitcherHand='R';
					
				}
				try {
					homePitcher = div.child(2).child(0).child(1).child(0).text();
					homePitcherHand = div.child(2).child(0).child(1).child(1).text().charAt(2);
				} catch (Exception e) {
					homePitcher = "N/A";
					homePitcherHand='R';
				}
				
				
				
				
				double finalHitterNumberAway1= 0, finalHitterNumberAway2= 0, finalHitterNumberAway3= 0, finalHitterNumberAway4= 0, finalHitterNumberAway5= 0, finalHitterNumberAway6= 0,
						finalHitterNumberAway7= 0, finalHitterNumberAway8= 0, finalHitterNumberAway9= 0, finalHitterNumberHome1 = 0, finalHitterNumberHome2= 0, finalHitterNumberHome3= 0,
						finalHitterNumberHome4= 0, finalHitterNumberHome5= 0, finalHitterNumberHome6= 0, finalHitterNumberHome7= 0, finalHitterNumberHome8= 0, finalHitterNumberHome9= 0;
				
				double finalHitterMatchupAway1=0, finalHitterMatchupAway2=0, finalHitterMatchupAway3=0, finalHitterMatchupAway4=0, finalHitterMatchupAway5=0, finalHitterMatchupAway6=0,
						finalHitterMatchupAway7=0, finalHitterMatchupAway8=0, finalHitterMatchupAway9=0, finalHitterMatchupHome1=0, finalHitterMatchupHome2=0, finalHitterMatchupHome3=0,
								finalHitterMatchupHome4=0, finalHitterMatchupHome5=0, finalHitterMatchupHome6=0, finalHitterMatchupHome7=0, finalHitterMatchupHome8=0, finalHitterMatchupHome9=0;
				double finalHitterAwayAvg, finalHitterHomeAvg;
				double finalAggregrateHitterNumberHome = 0, finalAggregrateHitterNumberAway = 0;
				double finalAggregratePitcherNumberHome = 0, finalAggregratePitcherNumberAway = 0;
				
				double awayTeamIndex, homeTeamIndex, parkIndex, awayPitcherIndex, homePitcherIndex;	
				
				if(mapTeamO.get(homeTeam) == null){
					homeTeamIndex = 0.0;
				}
				else {
					homeTeamIndex = mapTeamO.get(homeTeam);
				}
				
				if(mapPitcher.get(awayPitcher) ==null) {
					awayPitcherIndex = 0.0;
				}
				else {
					awayPitcherIndex = mapPitcher.get(awayPitcher);
				}
				if(mapTeamO.get(awayTeam) == null) {
					awayTeamIndex = 0.0;
				}
				else {
					awayTeamIndex = mapTeamO.get(awayTeam);
				}
				
				if(mapPF.get(park) == null) {
					parkIndex = 0.0;
				}
				else {
					parkIndex = mapPF.get(park);
				}
				
				if(mapPitcher.get(homePitcher) == null) {
					homePitcherIndex = 0.0;
				}
				else {
					homePitcherIndex = mapPitcher.get(homePitcher);
				}
				
			//	DecimalFormat df = new DecimalFormat("#.00");
				
				String homeHitters = null, awayHitters = null;
				if(div.child(1).child(1).child(0).text().equals("Lineup Pending...")) {}
				else {
					if(awayPitcherHand == 'R') {
						if(mapHitter.get(div.child(1).child(1).child(0).child(1).child(0).attr("title")) == null){
							finalHitterNumberHome1 = 0;
						}
						else {
							finalHitterNumberHome1 = mapHitter.get(div.child(1).child(1).child(0).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(1).child(0).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(1).child(0).child(1).child(0).attr("title")).getStdwRCPlusVSR());
							mapHitter.get(div.child(1).child(1).child(0).child(1).child(0).attr("title")).setPosition(div.child(1).child(1).child(0).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(1).child(0).child(1).child(0).attr("title")));
							finalHitterMatchupHome1 = finalHitterNumberHome1 + parkIndex - (awayPitcherIndex *.8);
							mapHitter.get(div.child(1).child(1).child(0).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupHome1);
							mapHitter.get(div.child(1).child(1).child(0).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(1).child(1).child(1).child(0).attr("title")) == null){
							finalHitterNumberHome2 = 0;
						}
						else {
							finalHitterNumberHome2 = mapHitter.get(div.child(1).child(1).child(1).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(1).child(1).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(1).child(1).child(1).child(0).attr("title")).getStdwRCPlusVSR());
							mapHitter.get(div.child(1).child(1).child(1).child(1).child(0).attr("title")).setPosition(div.child(1).child(1).child(1).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(1).child(1).child(1).child(0).attr("title")));
							finalHitterMatchupHome2 = finalHitterNumberHome2 + parkIndex - (awayPitcherIndex *.8);
							mapHitter.get(div.child(1).child(1).child(1).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupHome2);
							mapHitter.get(div.child(1).child(1).child(1).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(1).child(2).child(1).child(0).attr("title")) == null){
							finalHitterNumberHome3 = 0;
						}
						else {
							finalHitterNumberHome3 = mapHitter.get(div.child(1).child(1).child(2).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(1).child(2).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(1).child(2).child(1).child(0).attr("title")).getStdwRCPlusVSR());
							mapHitter.get(div.child(1).child(1).child(2).child(1).child(0).attr("title")).setPosition(div.child(1).child(1).child(2).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(1).child(2).child(1).child(0).attr("title")));
							finalHitterMatchupHome3 = finalHitterNumberHome3 + parkIndex - (awayPitcherIndex *.8);
							mapHitter.get(div.child(1).child(1).child(2).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupHome3);
							mapHitter.get(div.child(1).child(1).child(2).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(1).child(3).child(1).child(0).attr("title")) == null){
							finalHitterNumberHome4 = 0;
						}
						else {
							finalHitterNumberHome4 = mapHitter.get(div.child(1).child(1).child(3).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(1).child(3).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(1).child(3).child(1).child(0).attr("title")).getStdwRCPlusVSR());
							mapHitter.get(div.child(1).child(1).child(3).child(1).child(0).attr("title")).setPosition(div.child(1).child(1).child(3).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(1).child(3).child(1).child(0).attr("title")));
							finalHitterMatchupHome4 = finalHitterNumberHome4 + parkIndex - (awayPitcherIndex *.8);
							mapHitter.get(div.child(1).child(1).child(3).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupHome4);
							mapHitter.get(div.child(1).child(1).child(3).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(1).child(4).child(1).child(0).attr("title")) == null){
							finalHitterNumberHome5 = 0;

						}
						else {
							finalHitterNumberHome5 = mapHitter.get(div.child(1).child(1).child(4).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(1).child(4).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(1).child(4).child(1).child(0).attr("title")).getStdwRCPlusVSR());
							mapHitter.get(div.child(1).child(1).child(4).child(1).child(0).attr("title")).setPosition(div.child(1).child(1).child(4).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(1).child(4).child(1).child(0).attr("title")));
							finalHitterMatchupHome5 = finalHitterNumberHome5 + parkIndex - (awayPitcherIndex *.8);
							mapHitter.get(div.child(1).child(1).child(4).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupHome5);
							mapHitter.get(div.child(1).child(1).child(4).child(1).child(0).attr("title")).setTime(time);
						}
						if(mapHitter.get(div.child(1).child(1).child(5).child(1).child(0).attr("title")) == null){
							finalHitterNumberHome6 = 0;
						}
						else {
							finalHitterNumberHome6 = mapHitter.get(div.child(1).child(1).child(5).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(1).child(5).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(1).child(5).child(1).child(0).attr("title")).getStdwRCPlusVSR());
							mapHitter.get(div.child(1).child(1).child(5).child(1).child(0).attr("title")).setPosition(div.child(1).child(1).child(5).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(1).child(5).child(1).child(0).attr("title")));
						
							finalHitterMatchupHome6 = finalHitterNumberHome6 + parkIndex - (awayPitcherIndex *.8);
							mapHitter.get(div.child(1).child(1).child(5).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupHome6);
							mapHitter.get(div.child(1).child(1).child(5).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(1).child(6).child(1).child(0).attr("title")) == null){
							finalHitterNumberHome7 = 0;

						}
						else {
							finalHitterNumberHome7 = mapHitter.get(div.child(1).child(1).child(6).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(1).child(6).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(1).child(6).child(1).child(0).attr("title")).getStdwRCPlusVSR());
							mapHitter.get(div.child(1).child(1).child(6).child(1).child(0).attr("title")).setPosition(div.child(1).child(1).child(6).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(1).child(6).child(1).child(0).attr("title")));
						finalHitterMatchupHome7 = finalHitterNumberHome7 + parkIndex - (awayPitcherIndex *.8);
							mapHitter.get(div.child(1).child(1).child(6).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupHome7);
							mapHitter.get(div.child(1).child(1).child(6).child(1).child(0).attr("title")).setTime(time);
						}
						if(mapHitter.get(div.child(1).child(1).child(7).child(1).child(0).attr("title")) == null){
							finalHitterNumberHome8 = 0;

						}
						else {
							finalHitterNumberHome8 = mapHitter.get(div.child(1).child(1).child(7).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(1).child(7).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(1).child(7).child(1).child(0).attr("title")).getStdwRCPlusVSR());
							mapHitter.get(div.child(1).child(1).child(7).child(1).child(0).attr("title")).setPosition(div.child(1).child(1).child(7).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(1).child(7).child(1).child(0).attr("title")));
							finalHitterMatchupHome8 = finalHitterNumberHome8 + parkIndex - (awayPitcherIndex *.8);
							mapHitter.get(div.child(1).child(1).child(7).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupHome8);
							mapHitter.get(div.child(1).child(1).child(7).child(1).child(0).attr("title")).setTime(time);
						}
						if(mapHitter.get(div.child(1).child(1).child(8).child(1).child(0).attr("title")) == null){
							finalHitterNumberHome9 = 0;

						}
						else {
							finalHitterNumberHome9 = mapHitter.get(div.child(1).child(1).child(8).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(1).child(8).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(1).child(8).child(1).child(0).attr("title")).getStdwRCPlusVSR());
							mapHitter.get(div.child(1).child(1).child(8).child(1).child(0).attr("title")).setPosition(div.child(1).child(1).child(8).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(1).child(8).child(1).child(0).attr("title")));
							finalHitterMatchupHome9 = finalHitterNumberHome9 + parkIndex - (awayPitcherIndex *.8);
							mapHitter.get(div.child(1).child(1).child(8).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupHome9);
							mapHitter.get(div.child(1).child(1).child(8).child(1).child(0).attr("title")).setTime(time);
						}
						homeHitters = div.child(1).child(1).child(0).child(1).child(0).attr("title") + " - " + finalHitterNumberHome1 + " " + 
								div.child(1).child(1).child(1).child(1).child(0).attr("title") + " - " + finalHitterNumberHome2 + " " + 
								div.child(1).child(1).child(2).child(1).child(0).attr("title") + " - " + finalHitterNumberHome3 + " " + 
								div.child(1).child(1).child(3).child(1).child(0).attr("title") + " - " + finalHitterNumberHome4 + " " + 
								div.child(1).child(1).child(4).child(1).child(0).attr("title") + " - " + finalHitterNumberHome5 + " " + 
								div.child(1).child(1).child(5).child(1).child(0).attr("title") + " - " + finalHitterNumberHome6 + " " + 
								div.child(1).child(1).child(6).child(1).child(0).attr("title") + " - " + finalHitterNumberHome7 + " " + 
								div.child(1).child(1).child(7).child(1).child(0).attr("title") + " - " + finalHitterNumberHome8 + " " + 
								div.child(1).child(1).child(8).child(1).child(0).attr("title") + " - " + finalHitterNumberHome9;
						
						finalHitterHomeAvg = (finalHitterNumberHome1 + finalHitterNumberHome2 + finalHitterNumberHome3 + finalHitterNumberHome4 + finalHitterNumberHome5
								+ finalHitterNumberHome6 + finalHitterNumberHome7 + finalHitterNumberHome8 + finalHitterNumberHome9) / 9;
						
						
						
						finalAggregrateHitterNumberHome = finalHitterHomeAvg + parkIndex - (awayPitcherIndex * .8);
						finalAggregratePitcherNumberAway = (awayPitcherIndex * 1.2) - parkIndex - (finalHitterHomeAvg * .8);
						
					}
					else {
						if(mapHitter.get(div.child(1).child(1).child(0).child(1).child(0).attr("title")) == null){
							finalHitterNumberHome1 = 0;
						}
						else {
							finalHitterNumberHome1 = mapHitter.get(div.child(1).child(1).child(0).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(1).child(0).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(1).child(0).child(1).child(0).attr("title")).getStdwRCPlusVSL());
							mapHitter.get(div.child(1).child(1).child(0).child(1).child(0).attr("title")).setPosition(div.child(1).child(1).child(0).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(1).child(0).child(1).child(0).attr("title")));
							finalHitterMatchupHome1 = finalHitterNumberHome1 + parkIndex - (awayPitcherIndex *.8);
							mapHitter.get(div.child(1).child(1).child(0).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupHome1);
							mapHitter.get(div.child(1).child(1).child(0).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(1).child(1).child(1).child(0).attr("title")) == null){
							finalHitterNumberHome2 = 0;
						}
						else {
							
							finalHitterNumberHome2 = mapHitter.get(div.child(1).child(1).child(1).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(1).child(1).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(1).child(1).child(1).child(0).attr("title")).getStdwRCPlusVSL());
							mapHitter.get(div.child(1).child(1).child(1).child(1).child(0).attr("title")).setPosition(div.child(1).child(1).child(1).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(1).child(1).child(1).child(0).attr("title")));
							finalHitterMatchupHome2 = finalHitterNumberHome2 + parkIndex - (awayPitcherIndex *.8);
							mapHitter.get(div.child(1).child(1).child(1).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupHome2);
							mapHitter.get(div.child(1).child(1).child(1).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(1).child(2).child(1).child(0).attr("title")) == null){
							finalHitterNumberHome3 = 0;
						}
						else {
							
							finalHitterNumberHome3 = mapHitter.get(div.child(1).child(1).child(2).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(1).child(2).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(1).child(2).child(1).child(0).attr("title")).getStdwRCPlusVSL());
							mapHitter.get(div.child(1).child(1).child(2).child(1).child(0).attr("title")).setPosition(div.child(1).child(1).child(2).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(1).child(2).child(1).child(0).attr("title")));
							finalHitterMatchupHome3 = finalHitterNumberHome3 + parkIndex - (awayPitcherIndex *.8);
							mapHitter.get(div.child(1).child(1).child(2).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupHome3);
							mapHitter.get(div.child(1).child(1).child(2).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(1).child(3).child(1).child(0).attr("title")) == null){
							finalHitterNumberHome4 = 0;

						}
						else {
							finalHitterNumberHome4 = mapHitter.get(div.child(1).child(1).child(3).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(1).child(3).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(1).child(3).child(1).child(0).attr("title")).getStdwRCPlusVSL());
							mapHitter.get(div.child(1).child(1).child(3).child(1).child(0).attr("title")).setPosition(div.child(1).child(1).child(3).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(1).child(3).child(1).child(0).attr("title")));
							finalHitterMatchupHome4 = finalHitterNumberHome4 + parkIndex - (awayPitcherIndex *.8);
							mapHitter.get(div.child(1).child(1).child(3).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupHome4);
							mapHitter.get(div.child(1).child(1).child(3).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(1).child(4).child(1).child(0).attr("title")) == null){
							finalHitterNumberHome5 = 0;
						}
						else {
							finalHitterNumberHome5 = mapHitter.get(div.child(1).child(1).child(4).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(1).child(4).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(1).child(4).child(1).child(0).attr("title")).getStdwRCPlusVSL());
							mapHitter.get(div.child(1).child(1).child(4).child(1).child(0).attr("title")).setPosition(div.child(1).child(1).child(4).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(1).child(4).child(1).child(0).attr("title")));
							finalHitterMatchupHome5 = finalHitterNumberHome5 + parkIndex - (awayPitcherIndex *.8);
							mapHitter.get(div.child(1).child(1).child(4).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupHome5);
							mapHitter.get(div.child(1).child(1).child(4).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(1).child(5).child(1).child(0).attr("title")) == null){
							finalHitterNumberHome6 = 0;
						}
						else {
							
							finalHitterNumberHome6 = mapHitter.get(div.child(1).child(1).child(5).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(1).child(5).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(1).child(5).child(1).child(0).attr("title")).getStdwRCPlusVSL());
							mapHitter.get(div.child(1).child(1).child(5).child(1).child(0).attr("title")).setPosition(div.child(1).child(1).child(5).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(1).child(5).child(1).child(0).attr("title")));
							finalHitterMatchupHome6 = finalHitterNumberHome6 + parkIndex - (awayPitcherIndex *.8);
							mapHitter.get(div.child(1).child(1).child(5).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupHome6);
							mapHitter.get(div.child(1).child(1).child(5).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(1).child(6).child(1).child(0).attr("title")) == null){
							finalHitterNumberHome7 = 0;
						}
						else {
							finalHitterNumberHome7 = mapHitter.get(div.child(1).child(1).child(6).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(1).child(6).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(1).child(6).child(1).child(0).attr("title")).getStdwRCPlusVSL());
							mapHitter.get(div.child(1).child(1).child(6).child(1).child(0).attr("title")).setPosition(div.child(1).child(1).child(6).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(1).child(6).child(1).child(0).attr("title")));
							finalHitterMatchupHome7 = finalHitterNumberHome7 + parkIndex - (awayPitcherIndex *.8);
							mapHitter.get(div.child(1).child(1).child(6).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupHome7);
							mapHitter.get(div.child(1).child(1).child(6).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(1).child(7).child(1).child(0).attr("title")) == null){
							finalHitterNumberHome8 = 0;
						}
						else {
							
							finalHitterNumberHome8 = mapHitter.get(div.child(1).child(1).child(7).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(1).child(7).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(1).child(7).child(1).child(0).attr("title")).getStdwRCPlusVSL());
							mapHitter.get(div.child(1).child(1).child(7).child(1).child(0).attr("title")).setPosition(div.child(1).child(1).child(7).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(1).child(7).child(1).child(0).attr("title")));
							finalHitterMatchupHome8 = finalHitterNumberHome8 + parkIndex - (awayPitcherIndex *.8);
							mapHitter.get(div.child(1).child(1).child(7).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupHome8);
							mapHitter.get(div.child(1).child(1).child(7).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(1).child(8).child(1).child(0).attr("title")) == null){
							finalHitterNumberHome9 = 0;

						}
						else {
							finalHitterNumberHome9 = mapHitter.get(div.child(1).child(1).child(8).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(1).child(8).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(1).child(8).child(1).child(0).attr("title")).getStdwRCPlusVSL());
							mapHitter.get(div.child(1).child(1).child(8).child(1).child(0).attr("title")).setPosition(div.child(1).child(1).child(8).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(1).child(8).child(1).child(0).attr("title")));
							finalHitterMatchupHome9 = finalHitterNumberHome9 + parkIndex - (awayPitcherIndex *.8);
							mapHitter.get(div.child(1).child(1).child(8).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupHome9);
							mapHitter.get(div.child(1).child(1).child(8).child(1).child(0).attr("title")).setTime(time);
						}
						homeHitters = div.child(1).child(1).child(0).child(1).child(0).attr("title") + " - " + finalHitterNumberHome1 + " " + 
								div.child(1).child(1).child(1).child(1).child(0).attr("title") + " - " + finalHitterNumberHome2 + " " + 
								div.child(1).child(1).child(2).child(1).child(0).attr("title") + " - " + finalHitterNumberHome3 + " " + 
								div.child(1).child(1).child(3).child(1).child(0).attr("title") + " - " + finalHitterNumberHome4 + " " + 
								div.child(1).child(1).child(4).child(1).child(0).attr("title") + " - " + finalHitterNumberHome5 + " " + 
								div.child(1).child(1).child(5).child(1).child(0).attr("title") + " - " + finalHitterNumberHome6 + " " + 
								div.child(1).child(1).child(6).child(1).child(0).attr("title") + " - " + finalHitterNumberHome7 + " " + 
								div.child(1).child(1).child(7).child(1).child(0).attr("title") + " - " + finalHitterNumberHome8 + " " + 
								div.child(1).child(1).child(8).child(1).child(0).attr("title") + " - " + finalHitterNumberHome9;

						finalHitterHomeAvg = (finalHitterNumberHome1 + finalHitterNumberHome2 + finalHitterNumberHome3 + finalHitterNumberHome4 + finalHitterNumberHome5
								+ finalHitterNumberHome6 + finalHitterNumberHome7 + finalHitterNumberHome8 + finalHitterNumberHome9) / 9;
						
						
						
						finalAggregrateHitterNumberHome = finalHitterHomeAvg + parkIndex - (awayPitcherIndex * .8);
						finalAggregratePitcherNumberAway = (awayPitcherIndex * 1.2) - parkIndex - (finalHitterHomeAvg * .8);
						
					}
					
				}
				if(div.child(1).child(0).child(0).text().equals("Lineup Pending...")) {
		
				}
				else {
					if(homePitcherHand == 'R') {
						if(mapHitter.get(div.child(1).child(0).child(0).child(1).child(0).attr("title")) == null){
							finalHitterNumberAway1 = 0;
						}
						else {
							finalHitterNumberAway1 = mapHitter.get(div.child(1).child(0).child(0).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(0).child(0).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(0).child(0).child(1).child(0).attr("title")).getStdwRCPlusVSR());
							mapHitter.get(div.child(1).child(0).child(0).child(1).child(0).attr("title")).setPosition(div.child(1).child(0).child(0).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(0).child(0).child(1).child(0).attr("title")));
							finalHitterMatchupAway1 = finalHitterNumberAway1 + parkIndex - (homePitcherIndex *.8);
							mapHitter.get(div.child(1).child(0).child(0).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupAway1);
							mapHitter.get(div.child(1).child(0).child(0).child(1).child(0).attr("title")).setTime(time);
						}
						if(mapHitter.get(div.child(1).child(0).child(1).child(1).child(0).attr("title")) == null){
							finalHitterNumberAway2 = 0;
						}
						else {
						
							finalHitterNumberAway2 = mapHitter.get(div.child(1).child(0).child(1).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(0).child(1).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(0).child(1).child(1).child(0).attr("title")).getStdwRCPlusVSR());
							mapHitter.get(div.child(1).child(0).child(1).child(1).child(0).attr("title")).setPosition(div.child(1).child(0).child(1).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(0).child(1).child(1).child(0).attr("title")));
							
							finalHitterMatchupAway2 = finalHitterNumberAway2 + parkIndex - (homePitcherIndex *.8);
							mapHitter.get(div.child(1).child(0).child(1).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupAway2);
							mapHitter.get(div.child(1).child(0).child(1).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(0).child(2).child(1).child(0).attr("title")) == null){
							finalHitterNumberAway3 = 0;
						}
						else {
							finalHitterNumberAway3 = mapHitter.get(div.child(1).child(0).child(2).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(0).child(2).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(0).child(2).child(1).child(0).attr("title")).getStdwRCPlusVSR());
							mapHitter.get(div.child(1).child(0).child(2).child(1).child(0).attr("title")).setPosition(div.child(1).child(0).child(2).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(0).child(2).child(1).child(0).attr("title")));
							finalHitterMatchupAway3 = finalHitterNumberAway3 + parkIndex - (homePitcherIndex *.8);
							mapHitter.get(div.child(1).child(0).child(2).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupAway3);
							mapHitter.get(div.child(1).child(0).child(2).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(0).child(3).child(1).child(0).attr("title")) == null){
							finalHitterNumberAway4 = 0;
						}
						else {
						
							finalHitterNumberAway4 = mapHitter.get(div.child(1).child(0).child(3).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(0).child(3).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(0).child(3).child(1).child(0).attr("title")).getStdwRCPlusVSR());
							mapHitter.get(div.child(1).child(0).child(3).child(1).child(0).attr("title")).setPosition(div.child(1).child(0).child(3).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(0).child(3).child(1).child(0).attr("title")));
							finalHitterMatchupAway4 = finalHitterNumberAway4 + parkIndex - (homePitcherIndex *.8);
							mapHitter.get(div.child(1).child(0).child(3).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupAway4);
							mapHitter.get(div.child(1).child(0).child(3).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(0).child(4).child(1).child(0).attr("title")) == null){
							finalHitterNumberAway5 = 0;
						}
						else {
							
							finalHitterNumberAway5 = mapHitter.get(div.child(1).child(0).child(4).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(0).child(4).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(0).child(4).child(1).child(0).attr("title")).getStdwRCPlusVSR());
							mapHitter.get(div.child(1).child(0).child(4).child(1).child(0).attr("title")).setPosition(div.child(1).child(0).child(4).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(0).child(4).child(1).child(0).attr("title")));
						finalHitterMatchupAway5 = finalHitterNumberAway5 + parkIndex - (homePitcherIndex *.8);
							mapHitter.get(div.child(1).child(0).child(4).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupAway5);
							mapHitter.get(div.child(1).child(0).child(4).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(0).child(5).child(1).child(0).attr("title")) == null){
							finalHitterNumberAway6 = 0;
						}
						else {
							
							finalHitterNumberAway6 = mapHitter.get(div.child(1).child(0).child(5).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(0).child(5).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(0).child(5).child(1).child(0).attr("title")).getStdwRCPlusVSR());
							mapHitter.get(div.child(1).child(0).child(5).child(1).child(0).attr("title")).setPosition(div.child(1).child(0).child(5).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(0).child(5).child(1).child(0).attr("title")));
							finalHitterMatchupAway6 = finalHitterNumberAway6 + parkIndex - (homePitcherIndex *.8);
							mapHitter.get(div.child(1).child(0).child(5).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupAway6);
							mapHitter.get(div.child(1).child(0).child(5).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(0).child(6).child(1).child(0).attr("title")) == null){
							finalHitterNumberAway7 = 0;
						}
						else {
					
							finalHitterNumberAway7 = mapHitter.get(div.child(1).child(0).child(6).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(0).child(6).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(0).child(6).child(1).child(0).attr("title")).getStdwRCPlusVSR());
							mapHitter.get(div.child(1).child(0).child(6).child(1).child(0).attr("title")).setPosition(div.child(1).child(0).child(6).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(0).child(6).child(1).child(0).attr("title")));
						finalHitterMatchupAway7 = finalHitterNumberAway7 + parkIndex - (homePitcherIndex *.8);
							mapHitter.get(div.child(1).child(0).child(6).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupAway7);
							mapHitter.get(div.child(1).child(0).child(6).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(0).child(7).child(1).child(0).attr("title")) == null){
							finalHitterNumberAway8 = 0;
						}
						else {
						
							finalHitterNumberAway8 = mapHitter.get(div.child(1).child(0).child(7).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(0).child(7).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(0).child(7).child(1).child(0).attr("title")).getStdwRCPlusVSR());
							mapHitter.get(div.child(1).child(0).child(7).child(1).child(0).attr("title")).setPosition(div.child(1).child(0).child(7).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(0).child(7).child(1).child(0).attr("title")));
							finalHitterMatchupAway8 = finalHitterNumberAway8 + parkIndex - (homePitcherIndex *.8);
							mapHitter.get(div.child(1).child(0).child(7).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupAway8);
							mapHitter.get(div.child(1).child(0).child(7).child(1).child(0).attr("title")).setTime(time);
							
						}
						if(mapHitter.get(div.child(1).child(0).child(8).child(1).child(0).attr("title")) == null){
							finalHitterNumberAway9 = 0;
						}
						else {
						
							finalHitterNumberAway9 = mapHitter.get(div.child(1).child(0).child(8).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(0).child(8).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(0).child(8).child(1).child(0).attr("title")).getStdwRCPlusVSR());
							mapHitter.get(div.child(1).child(0).child(8).child(1).child(0).attr("title")).setPosition(div.child(1).child(0).child(8).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(0).child(8).child(1).child(0).attr("title")));
							finalHitterMatchupAway9 = finalHitterNumberAway9 + parkIndex - (homePitcherIndex *.8);
							mapHitter.get(div.child(1).child(0).child(8).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupAway9);
							mapHitter.get(div.child(1).child(0).child(8).child(1).child(0).attr("title")).setTime(time);
						}
						awayHitters = div.child(1).child(0).child(0).child(1).child(0).attr("title") + " - " + finalHitterNumberAway1 + " " + 
								div.child(1).child(0).child(1).child(1).child(0).attr("title") + " - " + finalHitterNumberAway2 + " " + 
								div.child(1).child(0).child(2).child(1).child(0).attr("title") + " - " + finalHitterNumberAway3 + " " + 
								div.child(1).child(0).child(3).child(1).child(0).attr("title") + " - " + finalHitterNumberAway4 + " " + 
								div.child(1).child(0).child(4).child(1).child(0).attr("title") + " - " + finalHitterNumberAway5 + " " + 
								div.child(1).child(0).child(5).child(1).child(0).attr("title") + " - " + finalHitterNumberAway6 + " " + 
								div.child(1).child(0).child(6).child(1).child(0).attr("title") + " - " + finalHitterNumberAway7 + " " + 
								div.child(1).child(0).child(7).child(1).child(0).attr("title") + " - " + finalHitterNumberAway8 + " " + 
								div.child(1).child(0).child(8).child(1).child(0).attr("title") + " - " + finalHitterNumberAway9;
						
						finalHitterAwayAvg = (finalHitterNumberAway1 + finalHitterNumberAway2 + finalHitterNumberAway3 + finalHitterNumberAway4 + finalHitterNumberAway5
								+ finalHitterNumberAway6 + finalHitterNumberAway7 + finalHitterNumberAway8 + finalHitterNumberAway9) / 9;
						
						finalAggregrateHitterNumberAway = finalHitterAwayAvg + parkIndex - (homePitcherIndex * .8);
						
						finalAggregratePitcherNumberHome = (homePitcherIndex * 1.2) - parkIndex - (finalHitterAwayAvg * .8);
										
					}
					else {
						if(mapHitter.get(div.child(1).child(0).child(0).child(1).child(0).attr("title")) == null){
							finalHitterNumberAway1 = 0;
						}
						else {
							finalHitterNumberAway1 = mapHitter.get(div.child(1).child(0).child(0).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(0).child(0).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(0).child(0).child(1).child(0).attr("title")).getStdwRCPlusVSL());
							mapHitter.get(div.child(1).child(0).child(0).child(1).child(0).attr("title")).setPosition(div.child(1).child(0).child(0).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(0).child(0).child(1).child(0).attr("title")));
							finalHitterMatchupAway1 = finalHitterNumberAway1 + parkIndex - (homePitcherIndex *.8);
							mapHitter.get(div.child(1).child(0).child(0).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupAway1);
							mapHitter.get(div.child(1).child(0).child(0).child(1).child(0).attr("title")).setTime(time);
						}
						if(mapHitter.get(div.child(1).child(0).child(1).child(1).child(0).attr("title")) == null){
							finalHitterNumberAway2 = 0;
						}
						else {
							
							finalHitterNumberAway2 = mapHitter.get(div.child(1).child(0).child(1).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(0).child(1).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(0).child(1).child(1).child(0).attr("title")).getStdwRCPlusVSL());
							mapHitter.get(div.child(1).child(0).child(1).child(1).child(0).attr("title")).setPosition(div.child(1).child(0).child(1).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(0).child(1).child(1).child(0).attr("title")));
							finalHitterMatchupAway2 = finalHitterNumberAway2 + parkIndex - (homePitcherIndex *.8);
							mapHitter.get(div.child(1).child(0).child(1).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupAway2);
							mapHitter.get(div.child(1).child(0).child(1).child(1).child(0).attr("title")).setTime(time);
						}
						if(mapHitter.get(div.child(1).child(0).child(2).child(1).child(0).attr("title")) == null){
							finalHitterNumberAway3 = 0;
						}
						else {
							finalHitterNumberAway3 = mapHitter.get(div.child(1).child(0).child(2).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(0).child(2).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(0).child(2).child(1).child(0).attr("title")).getStdwRCPlusVSL());
							mapHitter.get(div.child(1).child(0).child(2).child(1).child(0).attr("title")).setPosition(div.child(1).child(0).child(2).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(0).child(2).child(1).child(0).attr("title")));
							finalHitterMatchupAway3 = finalHitterNumberAway3 + parkIndex - (homePitcherIndex *.8);
							mapHitter.get(div.child(1).child(0).child(2).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupAway3);
							mapHitter.get(div.child(1).child(0).child(2).child(1).child(0).attr("title")).setTime(time);
						}
						if(mapHitter.get(div.child(1).child(0).child(3).child(1).child(0).attr("title")) == null){
							finalHitterNumberAway4 = 0;
						}
						else {
							
							finalHitterNumberAway4 = mapHitter.get(div.child(1).child(0).child(3).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(0).child(3).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(0).child(3).child(1).child(0).attr("title")).getStdwRCPlusVSL());
							mapHitter.get(div.child(1).child(0).child(3).child(1).child(0).attr("title")).setPosition(div.child(1).child(0).child(3).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(0).child(3).child(1).child(0).attr("title")));
							finalHitterMatchupAway4 = finalHitterNumberAway4 + parkIndex - (homePitcherIndex *.8);
							mapHitter.get(div.child(1).child(0).child(3).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupAway4);
							mapHitter.get(div.child(1).child(0).child(3).child(1).child(0).attr("title")).setTime(time);
						}
						if(mapHitter.get(div.child(1).child(0).child(4).child(1).child(0).attr("title")) == null){
							finalHitterNumberAway5 = 0;
						}
						else {
							finalHitterNumberAway5 = mapHitter.get(div.child(1).child(0).child(4).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(0).child(4).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(0).child(4).child(1).child(0).attr("title")).getStdwRCPlusVSL());
							mapHitter.get(div.child(1).child(0).child(4).child(1).child(0).attr("title")).setPosition(div.child(1).child(0).child(4).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(0).child(4).child(1).child(0).attr("title")));
							finalHitterMatchupAway5 = finalHitterNumberAway5 + parkIndex - (homePitcherIndex *.8);
							mapHitter.get(div.child(1).child(0).child(4).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupAway5);
							mapHitter.get(div.child(1).child(0).child(4).child(1).child(0).attr("title")).setTime(time);
						}
						if(mapHitter.get(div.child(1).child(0).child(5).child(1).child(0).attr("title")) == null){
							finalHitterNumberAway6 = 0;
						}
						else {
							finalHitterNumberAway6 = mapHitter.get(div.child(1).child(0).child(5).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(0).child(5).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(0).child(5).child(1).child(0).attr("title")).getStdwRCPlusVSL());
							mapHitter.get(div.child(1).child(0).child(5).child(1).child(0).attr("title")).setPosition(div.child(1).child(0).child(5).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(0).child(5).child(1).child(0).attr("title")));
							finalHitterMatchupAway6 = finalHitterNumberAway6 + parkIndex - (homePitcherIndex *.8);
							mapHitter.get(div.child(1).child(0).child(5).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupAway6);
							mapHitter.get(div.child(1).child(0).child(5).child(1).child(0).attr("title")).setTime(time);
						}
						if(mapHitter.get(div.child(1).child(0).child(6).child(1).child(0).attr("title")) == null){
							finalHitterNumberAway7 = 0;
						}
						else {
							
							finalHitterNumberAway7 = mapHitter.get(div.child(1).child(0).child(6).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(0).child(6).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(0).child(6).child(1).child(0).attr("title")).getStdwRCPlusVSL());
							mapHitter.get(div.child(1).child(0).child(6).child(1).child(0).attr("title")).setPosition(div.child(1).child(0).child(6).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(0).child(6).child(1).child(0).attr("title")));							
							finalHitterMatchupAway7 = finalHitterNumberAway7 + parkIndex - (homePitcherIndex *.8);
							mapHitter.get(div.child(1).child(0).child(6).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupAway7);
							mapHitter.get(div.child(1).child(0).child(6).child(1).child(0).attr("title")).setTime(time);
						}
						if(mapHitter.get(div.child(1).child(0).child(7).child(1).child(0).attr("title")) == null){
							finalHitterNumberAway8 = 0;
						}
						else {
							finalHitterNumberAway8 = mapHitter.get(div.child(1).child(0).child(7).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(0).child(7).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(0).child(7).child(1).child(0).attr("title")).getStdwRCPlusVSL());
							mapHitter.get(div.child(1).child(0).child(7).child(1).child(0).attr("title")).setPosition(div.child(1).child(0).child(7).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(0).child(7).child(1).child(0).attr("title")));
							finalHitterMatchupAway8 = finalHitterNumberAway8 + parkIndex - (homePitcherIndex *.8);
							mapHitter.get(div.child(1).child(0).child(7).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupAway8);
							mapHitter.get(div.child(1).child(0).child(7).child(1).child(0).attr("title")).setTime(time);
						}
						if(mapHitter.get(div.child(1).child(0).child(8).child(1).child(0).attr("title")) == null){
							finalHitterNumberAway9 = 0;
						}
						else {
							finalHitterNumberAway9 = mapHitter.get(div.child(1).child(0).child(8).child(1).child(0).attr("title")).calculateFinalwRCPlusDaily(mapHitter.get(div.child(1).child(0).child(8).child(1).child(0).attr("title")).getStdwRCPlus(), mapHitter.get(div.child(1).child(0).child(8).child(1).child(0).attr("title")).getStdwRCPlusVSL());
							mapHitter.get(div.child(1).child(0).child(8).child(1).child(0).attr("title")).setPosition(div.child(1).child(0).child(8).child(0).text());
							finalHitterSet.add(mapHitter.get(div.child(1).child(0).child(8).child(1).child(0).attr("title")));
							finalHitterMatchupAway9 = finalHitterNumberAway9 + parkIndex - (homePitcherIndex *.8);
							mapHitter.get(div.child(1).child(0).child(8).child(1).child(0).attr("title")).setFinalMatchupValue(finalHitterMatchupAway9);
							mapHitter.get(div.child(1).child(0).child(8).child(1).child(0).attr("title")).setTime(time);
						}
				
						awayHitters = div.child(1).child(0).child(0).child(1).child(0).attr("title") + " - " + finalHitterNumberAway1 + " " + 
								div.child(1).child(0).child(1).child(1).child(0).attr("title") + " - " + finalHitterNumberAway2 + " " + 
								div.child(1).child(0).child(2).child(1).child(0).attr("title") + " - " + finalHitterNumberAway3 + " " + 
								div.child(1).child(0).child(3).child(1).child(0).attr("title") + " - " + finalHitterNumberAway4 + " " + 
								div.child(1).child(0).child(4).child(1).child(0).attr("title") + " - " + finalHitterNumberAway5 + " " + 
								div.child(1).child(0).child(5).child(1).child(0).attr("title") + " - " + finalHitterNumberAway6 + " " + 
								div.child(1).child(0).child(6).child(1).child(0).attr("title") + " - " + finalHitterNumberAway7 + " " + 
								div.child(1).child(0).child(7).child(1).child(0).attr("title") + " - " + finalHitterNumberAway8 + " " + 
								div.child(1).child(0).child(8).child(1).child(0).attr("title") + " - " + finalHitterNumberAway9;

						finalHitterAwayAvg = (finalHitterNumberAway1 + finalHitterNumberAway2 + finalHitterNumberAway3 + finalHitterNumberAway4 + finalHitterNumberAway5
								+ finalHitterNumberAway6 + finalHitterNumberAway7 + finalHitterNumberAway8 + finalHitterNumberAway9) / 9;
						
						finalAggregrateHitterNumberAway = finalHitterAwayAvg + parkIndex - (homePitcherIndex * .8);
						finalAggregratePitcherNumberHome = (homePitcherIndex * 1.2) - parkIndex - (finalHitterAwayAvg * .8);
											
					}
						
				}		
				
				double awayPitcherNumber = (awayPitcherIndex * 1.2) - parkIndex - (homeTeamIndex * .8);
				double homeTeamNumber = homeTeamIndex + parkIndex  - (awayPitcherIndex *.8);
				double homePitcherNumber = (homePitcherIndex *1.2) - parkIndex - (awayTeamIndex * .8) ;
				double awayTeamNumber = awayTeamIndex + parkIndex  - (homePitcherIndex *.8);
				
				ArrayList<Object> homePitcherStatList = new ArrayList<Object>();
				ArrayList<Object> awayPitcherStatList = new ArrayList<Object>();
				ArrayList<Object> homeTeamStatList = new ArrayList<Object>();
				ArrayList<Object> awayTeamStatList = new ArrayList<Object>();
				
				homePitcherStatList.add(homePitcherNumber);
				homePitcherStatList.add(finalAggregratePitcherNumberHome);
				
				awayPitcherStatList.add(awayPitcherNumber);
				awayPitcherStatList.add(finalAggregratePitcherNumberAway);
				
				awayTeamStatList.add(awayTeamNumber);
				awayTeamStatList.add(finalAggregrateHitterNumberAway);
				
				homeTeamStatList.add(homeTeamNumber);
				homeTeamStatList.add(finalAggregrateHitterNumberHome);
				
				FinalNumberObject finalAwayPitcher = new FinalNumberObject(awayPitcher, awayPitcherStatList);
				FinalNumberObject finalHomeTeam = new FinalNumberObject(homeTeam, homeTeamStatList);
				FinalNumberObject finalHomePitcher = new FinalNumberObject(homePitcher, homePitcherStatList);
				FinalNumberObject finalAwayTeam = new FinalNumberObject(awayTeam, awayTeamStatList);
				
				finalAwayPitcher.addValue(time);
				finalHomePitcher.addValue(time);
				finalAwayTeam.addValue(time);
				finalHomeTeam.addValue(time);
				
				finalPitcherSet.add(finalAwayPitcher);
				finalPitcherSet.add(finalHomePitcher);
				finalTeamSet.add(finalAwayTeam);
				finalTeamSet.add(finalHomeTeam);				
			
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private static void calculatePitcherDeviations(TreeSet<Pitcher> pitcherSet) {
		DescriptiveStatistics stats = new DescriptiveStatistics();
		DescriptiveStatistics statsxFipMinus = new DescriptiveStatistics();
		DescriptiveStatistics statskPerc = new DescriptiveStatistics();
		
		Iterator<Pitcher> it = pitcherSet.iterator();
		
		while (it.hasNext()) {
			Pitcher p = it.next();
			stats.addValue(p.getSiera());
			statsxFipMinus.addValue(p.getxFIPMinus());
			statskPerc.addValue(p.getkPerc());
		}
		
		double mean = stats.getMean();
		double std = stats.getStandardDeviation();
		double meanXFipMinus = statsxFipMinus.getMean();
		double stdXFipMinus = statsxFipMinus.getStandardDeviation();
		double meanKPerc = statskPerc.getMean();
		double stdKPerc = statskPerc.getStandardDeviation();
		
		Iterator<Pitcher> it2 = pitcherSet.iterator();
	
		while (it2.hasNext()) {
			Pitcher p = it2.next();
			p.calculateSieraSTD(mean, std);
			p.calculatexFipMinusSTD(meanXFipMinus, stdXFipMinus);
			p.calculateKPercSTD(meanKPerc, stdKPerc);
			p.calculateFinalStd(p.getStdSierra(), p.getStdXFipMinus(), p.getStdKPerc());
		}
	}

	private static void getPitcherStats(TreeSet<Pitcher> pitcherSet) {
		try {
			Document doc = Jsoup.connect("http://www.fangraphs.com/leaders.aspx?pos=all&stats=sta&lg=all&qual=0&type=1&season=2015&month=0&season1=2015&ind=0&team=0&rost=0&age=0&filter=&players=0&sort=21,a&page=1_500").get();
			Elements tableRow = doc.getElementsByClass("rgRow");
			Elements tableRow2 = doc.getElementsByClass("rgAltRow");
			tableRow.addAll(tableRow2);
			for(int i = 0; i < tableRow.size(); i++){
				double value = Double.parseDouble(tableRow.get(i).child(7).text().substring(0, tableRow.get(i).child(7).text().indexOf('%')-1));
				Pitcher p = new Pitcher(tableRow.get(i).child(1).text(), value, Double.parseDouble(tableRow.get(i).child(11).text()), Double.parseDouble(tableRow.get(i).child(12).text()), Double.parseDouble(tableRow.get(i).child(16).text()), Double.parseDouble(tableRow.get(i).child(21).text()));
				pitcherSet.add(p);
				
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void calculateTeamHittingDeviations(TreeSet<Team> teamSet) {
		DescriptiveStatistics stats = new DescriptiveStatistics();
		
		Iterator<Team> it = teamSet.iterator();
		
		while (it.hasNext()) {
			stats.addValue(it.next().getwRCPlus());
		}
		
		double mean = stats.getMean();
		double std = stats.getStandardDeviation();
		
		Iterator<Team> it2 = teamSet.iterator();
	
		while (it2.hasNext()) {
			Team t = it2.next();
			t.calculateWRCPlusSTD(mean, std);
			//System.out.println(t.toString());
		}
		
	}

	private static void getTeamHittingStats(TreeSet<Team> teamSet) {
		try {
			Document doc = Jsoup.connect("http://www.fangraphs.com/leaders.aspx?pos=all&stats=bat&lg=all&qual=0&type=1&season=2015&month=0&season1=2015&ind=0&team=0,ts&rost=0&age=0&filter=&players=0&sort=19,d").get();
			
			Elements tableRow = doc.getElementsByClass("rgRow");
			Elements tableRow2 = doc.getElementsByClass("rgAltRow");
			tableRow.addAll(tableRow2);
			for(int i = 0; i < tableRow.size(); i++){
				Team t = new Team(tableRow.get(i).child(1).text(), Double.parseDouble(tableRow.get(i).child(19).text()), Double.parseDouble(tableRow.get(i).child(18).text()), Double.parseDouble(tableRow.get(i).child(17).text()));
				teamSet.add(t);
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private static void getPFStats(TreeSet<ParkFactor> set) {
		DescriptiveStatistics statsL = new DescriptiveStatistics();
		DescriptiveStatistics statsR = new DescriptiveStatistics();
		DescriptiveStatistics statsT = new DescriptiveStatistics();
		
		Iterator<ParkFactor> it = set.iterator();
		Iterator<ParkFactor> it2 = set.iterator();
		Iterator<ParkFactor> it3 = set.iterator();
		
		while (it.hasNext()) {
			statsL.addValue(it.next().getWeightedL());
		}
		while (it2.hasNext()) {
			statsR.addValue(it2.next().getWeightedR());
		}
		while (it3.hasNext()) {
			statsT.addValue(it3.next().getWeightedT());
		}

		double meanL = statsL.getMean();
		double stdL = statsL.getStandardDeviation();
		double meanR = statsR.getMean();
		double stdR = statsR.getStandardDeviation();
		double meanT = statsT.getMean();
		double stdT = statsT.getStandardDeviation();
		
		Iterator<ParkFactor> it4 = set.iterator();
		while (it4.hasNext()) {
			ParkFactor p = it4.next();
			p.calculateStds(meanL, stdL, meanR, stdR, meanT, stdT);
		}
		
	}

	private static void printMap(Map<String, Integer> map) {
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
		    String key = entry.getKey();
		    Integer value = entry.getValue();
		    System.out.println(key + ": " + value);
		}
	}

	private static void getParkFactors(TreeSet<ParkFactor> set) {
		
		try {
			Document doc = Jsoup.connect("http://www.fangraphs.com/guts.aspx?type=pfh&teamid=0&season=2014").get();
			Elements table = doc.getElementsByTag("tbody");
			Elements tableRow = table.get(17).getElementsByTag("tr");
			for(int i = 0; i < tableRow.size(); i++){
				ParkFactor p = new ParkFactor(tableRow.get(i).child(1).text(), Double.parseDouble(tableRow.get(i).child(2).text()),Double.parseDouble(tableRow.get(i).child(3).text()),Double.parseDouble(tableRow.get(i).child(4).text()),Double.parseDouble(tableRow.get(i).child(5).text()),Double.parseDouble(tableRow.get(i).child(6).text()),Double.parseDouble(tableRow.get(i).child(7).text()),Double.parseDouble(tableRow.get(i).child(8).text()),Double.parseDouble(tableRow.get(i).child(9).text()));
				set.add(p);
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
